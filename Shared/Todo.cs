﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoDeploiementVPS.Shared
{
    public class Todo
    {
        [Key]
        public Guid Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public bool Complete { get; set; }
    }
}
