﻿using DemoDeploiementVPS.Shared;
using Microsoft.EntityFrameworkCore;

namespace DemoDeploiementVPS.Server.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(
            DbContextOptions options) : base(options)
        {
        }

        public DbSet<Todo> Todos { get; set; }
    }
}
