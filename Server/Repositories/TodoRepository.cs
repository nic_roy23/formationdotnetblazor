﻿using DemoDeploiementVPS.Server.Database;
using DemoDeploiementVPS.Server.Interfaces;
using DemoDeploiementVPS.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Server.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        public readonly AppDbContext _context;

        public TodoRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Todo> CompleteTodo(Guid id)
        {
            var todoTrouve = await _context.Todos.FirstAsync(t => t.Id == id);
            if (todoTrouve != null)
            {
                todoTrouve.Complete = true;

                var todoUpdate = _context.Todos.Update(todoTrouve);
                await _context.SaveChangesAsync();

                return todoUpdate.Entity;
            }

            return new Todo();
        }

        public async Task<Todo> CreateTodo(Todo todo)
        {
            var todoRetour = await _context.Todos.AddAsync(todo);
            await _context.SaveChangesAsync();

            return todoRetour.Entity;
        }

        public async Task<Todo> DeleteTodo(Guid id)
        {
            var todoTrouve = await _context.Todos.FirstAsync(t => t.Id == id);
            if (todoTrouve != null)
            {
                var todoSupprime = _context.Todos.Remove(todoTrouve);
                await _context.SaveChangesAsync();

                return todoSupprime.Entity;
            }

            return new Todo();
        }

        public async Task<Todo> GetTodo(Guid id)
        {
            return await _context.Todos.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<List<Todo>> GetTodos()
        {
            return await _context.Todos.ToListAsync();
        }
    }
}
