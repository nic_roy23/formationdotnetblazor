﻿using DemoDeploiementVPS.Server.Interfaces;
using DemoDeploiementVPS.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Server.Controllers
{
    [Route("api/todos")]
    public class TodoController : ControllerBase
    {
        public readonly ITodoRepository _todoRepository;

        public TodoController(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Todo>>> GetTodos()
        {
            try
            {
                var todos = await _todoRepository.GetTodos();
                return Ok(todos);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(StatusCodes.Status500InternalServerError, "Erreur avec l'obtention des tâches.");
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Todo>> GetTodo(Guid id)
        {
            try
            {
                var todo = await _todoRepository.GetTodo(id);
                return Ok(todo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(StatusCodes.Status500InternalServerError, "Erreur avec l'obtention des tâches.");
            }
        }

        [HttpPost]
        public async Task<ActionResult<Todo>> NewTodo([FromBody] Todo todo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                todo.Id = Guid.NewGuid();
                todo.Date = DateTime.Now;
                todo.Complete = false;

                var todoRetour = await _todoRepository.CreateTodo(todo);

                return Created("todos", todoRetour);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(StatusCodes.Status500InternalServerError, "Erreur avec l'obtention des tâches.");
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult<Todo>> DeleteTodo(Guid id)
        {
            try
            {
                var todoSupprime = await _todoRepository.DeleteTodo(id);
                return Ok(todoSupprime);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(StatusCodes.Status500InternalServerError, "Erreur avec l'obtention des tâches.");
            }
        }

        [HttpPatch]
        [Route("{id}")]
        public async Task<ActionResult<Todo>> CompleteTodo(Guid id)
        {
            try
            {
                var todoComplete = await _todoRepository.CompleteTodo(id);
                return Ok(todoComplete);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(StatusCodes.Status500InternalServerError, "Erreur avec l'obtention des tâches.");
            }
        }
    }
}
