﻿using DemoDeploiementVPS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Server.Interfaces
{
    public interface ITodoRepository
    {
        public Task<List<Todo>> GetTodos();
        public Task<Todo> CreateTodo(Todo todo);
        public Task<Todo> GetTodo(Guid id);
        public Task<Todo> DeleteTodo(Guid id);
        public Task<Todo> CompleteTodo(Guid id);
    }
}
