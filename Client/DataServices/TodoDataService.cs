﻿using DemoDeploiementVPS.Client.Interfaces;
using DemoDeploiementVPS.Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Client.DataServices
{
    public class TodoDataService : ITodoDataService
    {
        private readonly HttpClient _httpClient;

        public TodoDataService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task CompleteTodo(Guid id)
        {
            await _httpClient.PatchAsync($"api/todos/{id}", null);
        }

        public async Task CreateTodo(Todo todo)
        {
            await _httpClient.PostAsJsonAsync("api/todos", todo);
        }

        public async Task DeleteTodo(Guid id)
        {
            await _httpClient.DeleteAsync($"api/todos/{id}");
        }

        public async Task<Todo> GetTodo(Guid id)
        {
            return await _httpClient.GetFromJsonAsync<Todo>($"api/todos{id}");
        }

        public async Task<List<Todo>> GetTodos()
        {
            return await _httpClient.GetFromJsonAsync<List<Todo>>("api/todos");
        }
    }
}
