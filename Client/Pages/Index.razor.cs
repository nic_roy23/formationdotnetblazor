﻿using DemoDeploiementVPS.Client.Interfaces;
using DemoDeploiementVPS.Shared;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Client.Pages
{
    public partial class Index
    {
		[Inject]
		public ITodoDataService TodoDataService { get; set; }

        public Todo TodoCourant { get; set; } = new Todo();
        public List<Todo> Todos { get; set; } = new List<Todo>();

		protected override async Task OnInitializedAsync()
		{
			Todos = await TodoDataService.GetTodos();
		}

		public async void HandleValidSubmit()
		{
			await TodoDataService.CreateTodo(TodoCourant);
			MAJTaches();
		}

		public void HandleInvalidSubmit()
		{

		}

		public async void CompleterTodo(Guid idTodo)
		{
			await TodoDataService.CompleteTodo(idTodo);
			MAJTaches();
		}

		public async void SupprimerTodo(Guid idTodo)
		{
			await TodoDataService.DeleteTodo(idTodo);
			MAJTaches();
		}

		public async void MAJTaches()
        {
			Todos = await TodoDataService.GetTodos();
			StateHasChanged();
		}
    }
}
