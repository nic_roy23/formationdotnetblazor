﻿using DemoDeploiementVPS.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoDeploiementVPS.Client.Interfaces
{
    public interface ITodoDataService
    {
        public Task<List<Todo>> GetTodos();
        public Task CreateTodo(Todo todo);
        public Task<Todo> GetTodo(Guid id);
        public Task DeleteTodo(Guid id);
        public Task CompleteTodo(Guid id);
    }
}
